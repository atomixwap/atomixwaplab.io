.PHONY: clean
clean:
	rm -rf public

.PHONY: setup
setup:
	npm install

public:
	npm run build

.PHONY: build
build: clean public
